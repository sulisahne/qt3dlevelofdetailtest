#include "Disc.h"
#include "Plane.h"
#include "Ray.h"

namespace ibh {

Disc::Disc(const QVector3D& origin, const QVector3D& normal, float radius)
    : _origin(origin)
    , _normal(normal)
    , _radius(radius)
{
}

void Disc::normalize()
{
    const auto norm = _normal.length();
    if (norm == 0.0f)
        return;

    _normal /= norm;
    _radius /= norm;
}

QVector3D Disc::intersect(const Ray& ray) const
{
    Plane plane { _origin, _normal };
    const auto p = plane.intersect(ray);
    if (p.isNull())
        return {}; // Keine Schnittpunkt. Ebene und Gerade sind parallel.

    const auto dist2 = (p - _origin).lengthSquared();
    if (dist2 > _radius * _radius)
        return {}; // Schnittpunkt außerhalb der Scheibe

    return p;
}
}
