#pragma once
#include "Layers.h"
#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>

namespace Qt3DCore {
class QNode;
class QTransform;
}
namespace Qt3DRender {
class QCamera;
}
namespace ibh {
class GroundEntity : public Qt3DCore::QEntity {
    Q_OBJECT
    Q_PROPERTY(Qt3DRender::QCamera* camera READ camera WRITE setCamera NOTIFY cameraChanged)
    Q_PROPERTY(Layers* layers READ layers WRITE setLayers NOTIFY layersChanged)

private:
    Qt3DRender::QCamera* m_camera = nullptr;
    Qt3DCore::QTransform* m_transform = nullptr;
    Layers* m_layers = nullptr;
    QMetaObject::Connection m_camPosConnection;

public:
    GroundEntity(Qt3DCore::QEntity* parent = nullptr);
    Qt3DRender::QCamera* camera() const;
    Layers* layers() const;

public slots:
    void setCamera(Qt3DRender::QCamera* camera);
    void setLayers(Layers* layers);

signals:
    void cameraChanged(Qt3DRender::QCamera* camera);
    void layersChanged(Layers* layers);
};
}
