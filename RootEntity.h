#pragma once
#include "Layers.h"
#include "Materials.h"
#include "Meshes.h"
#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>

namespace Qt3DCore {
class QNode;
}
namespace ibh {
class RootEntity : public Qt3DCore::QEntity {
    Q_OBJECT
    Q_PROPERTY(Qt3DRender::QCamera* camera READ camera WRITE setCamera NOTIFY cameraChanged)
    Q_PROPERTY(Layers* layers READ layers WRITE setLayers NOTIFY layersChanged)

private:
    Meshes m_meshes;
    Materials m_materials;
    Qt3DRender::QCamera* m_camera = nullptr;
    Layers* m_layers = nullptr;

public:
    RootEntity(Qt3DCore::QNode* parent = nullptr);
    Qt3DRender::QCamera* camera() const;
    Layers* layers() const;

public slots:
    void onCompleted();
    void setCamera(Qt3DRender::QCamera* camera);
    void setLayers(Layers* layers);

signals:
    void completed();
    void cameraChanged(Qt3DRender::QCamera* camera);
    void layersChanged(Layers* layers);

private:
    void init();
};
}
