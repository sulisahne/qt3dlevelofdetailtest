#include "Meshes.h"
#include "LineMesh.h"
#include <Qt3DExtras/QCylinderMesh>

namespace ibh {
Qt3DExtras::QCylinderMesh* Meshes::cylinderHD()
{
    if (!m_cylinderHD) {
        m_cylinderHD = new Qt3DExtras::QCylinderMesh();
        m_cylinderHD->setRings(20);
        m_cylinderHD->setSlices(30);
        m_cylinderHD->setLength(1);
        m_cylinderHD->setRadius(1);
    }

    return m_cylinderHD;
}

Qt3DExtras::QCylinderMesh* Meshes::cylinderMD()
{
    if (!m_cylinderMD) {
        m_cylinderMD = new Qt3DExtras::QCylinderMesh();
        m_cylinderMD->setRings(4);
        m_cylinderMD->setSlices(10);
        m_cylinderMD->setLength(1);
        m_cylinderMD->setRadius(1);
    }

    return m_cylinderMD;
}

Qt3DExtras::QCylinderMesh* Meshes::cylinderLD()
{
    if (!m_cylinderLD) {
        m_cylinderLD = new Qt3DExtras::QCylinderMesh();
        m_cylinderLD->setRings(2);
        m_cylinderLD->setSlices(2);
        m_cylinderLD->setLength(1);
        m_cylinderLD->setRadius(1);
    }

    return m_cylinderLD;
}

QVector<Qt3DExtras::QCylinderMesh*> Meshes::cylinderMeshes()
{
    return { cylinderHD(), cylinderMD(), cylinderLD() };
}

LineMesh* Meshes::line()
{
    if (!m_line)
        m_line = new LineMesh();

    return m_line;
}

}
