#include "CylinderEntity.h"
#include "Materials.h"
#include "Meshes.h"
#include <Qt3DCore/QNode>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QLayer>
#include <Qt3DRender/QLevelOfDetail>

namespace ibh {

CylinderEntity::CylinderEntity(Qt3DCore::QNode* parent)
    : Qt3DCore::QEntity(parent)
{
    init();
}

void CylinderEntity::setMesh(Qt3DExtras::QCylinderMesh* mesh)
{
    if (m_mesh == mesh)
        return;

    if (m_mesh)
        removeComponent(m_mesh);

    m_mesh = mesh;
    addComponent(m_mesh);
}

void CylinderEntity::setMeshes(const QVector<Qt3DExtras::QCylinderMesh*>& meshes)
{
    m_meshes = meshes;
    if (m_meshes.empty())
        return;

    setMesh(m_meshes.last());
}

void CylinderEntity::setMaterial(Qt3DExtras::QPhongMaterial* material)
{
    if (m_material == material)
        return;

    if (m_material)
        removeComponent(m_material);

    m_material = material;
    addComponent(m_material);
}

void CylinderEntity::setMaterials(const QVector<Qt3DExtras::QPhongMaterial*>& materials)
{
    m_materials = materials;
    if (m_materials.empty())
        return;

    setMaterial(m_materials.last());
}

void CylinderEntity::addLayer(Qt3DRender::QLayer* layer)
{
    addComponent(layer);
}

void CylinderEntity::setTranslation(const QVector3D& translation)
{
    if (getTranslation() == translation)
        return;

    m_transform->setTranslation(translation);
}

void CylinderEntity::move(const QVector3D& translation)
{
    m_transform->setTranslation(m_transform->translation() + translation);
}

void CylinderEntity::setRotation(const QQuaternion& rotation)
{
    if (qFuzzyCompare(getRotation(), rotation))
        return;

    m_transform->setRotation(rotation);
}

void CylinderEntity::setDiameter(float diameter)
{
    if (qFuzzyCompare(getDiameter(), diameter))
        return;

    auto scale = m_transform->scale3D();
    scale.setX(diameter);
    scale.setZ(diameter);
    m_transform->setScale3D(scale);
}

void CylinderEntity::setHeight(float height)
{
    if (qFuzzyCompare(getHeight(), height))
        return;

    auto scale = m_transform->scale3D();
    scale.setY(height);
    m_transform->setScale3D(scale);
}

void CylinderEntity::setLevelOfDetailEnabled(Qt3DRender::QCamera* camera)
{
    if (!m_levelOfDetail) {
        m_levelOfDetail = new Qt3DRender::QLevelOfDetail();
        addComponent(m_levelOfDetail);
        connect(m_levelOfDetail, &Qt3DRender::QLevelOfDetail::currentIndexChanged, [this, camera](int index) {
            //qDebug() << "LOD: " << index << " DIST: " << camera->position().length();
            setMaterial(m_materials[index]);
            setMesh(m_meshes[index]);
        });
    }

    m_levelOfDetail->setCamera(camera);
    m_levelOfDetail->setThresholds({ 50, 100, 150 });
    m_levelOfDetail->setThresholdType(Qt3DRender::QLevelOfDetail::ThresholdType::DistanceToCameraThreshold);
    //m_levelOfDetail->setVolumeOverride(m_levelOfDetail->createBoundingSphere({0,0,0}, 1.0));
}

Qt3DExtras::QCylinderMesh* CylinderEntity::getMesh() const
{
    return m_mesh;
}

QVector3D CylinderEntity::getTranslation() const
{
    return m_transform->translation();
}

QQuaternion CylinderEntity::getRotation() const
{
    return m_transform->rotation();
}

float CylinderEntity::getDiameter() const
{
    return m_transform->scale3D().x();
}

float CylinderEntity::getHeight() const
{
    return m_transform->scale3D().y();
}

void CylinderEntity::init()
{
    m_transform = new Qt3DCore::QTransform();
    addComponent(m_transform);
}
}
