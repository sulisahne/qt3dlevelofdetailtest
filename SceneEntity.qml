import QtQuick 2.12
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Extras 2.12
import Qt3D.Input 2.12
import Qt3D.Logic 2.12
import custom 1.0

Entity {
    id: root
    property int frameCount: 0
    property Entity selectedEntity
    property rect viewport: Qt.rect(0, 0, 0, 0)

    Layers {
        id: layers
        debugLayer: Layer {
            id: debugLayer
            recursive: true
        }
        objectLayer: Layer {
            id: objectLayer
            recursive: false
        }
        sceneLayer: Layer {
            id: sceneLayer
            recursive: false
        }
        osmLayer: Layer {
            id: osmLayer
            recursive: false
        }
        dxfLayer: Layer {
            id: dxfLayer
            recursive: false
        }
        flagLayer: Layer {
            id: flagLayer
            recursive: false
        }
    }

    FrameAction {
        onTriggered: root.frameCount++
    }

    Camera {
        id: mainCamera
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        //aspectRatio: 16/9
        nearPlane: 0.1
        farPlane: 10000.0
        position: Qt.vector3d(0.0, 0.0, 50.0)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
        viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
    }

    RenderSettings {
        id: renderSettings
        renderPolicy: RenderSettings.OnDemand
        pickingSettings.pickMethod: PickingSettings.TrianglePicking
        pickingSettings.faceOrientationPickingMode: PickingSettings.FrontFace
        pickingSettings.pickResultMode: PickingSettings.NearestPick
        activeFrameGraph: MyFrameGraph {
            camera: mainCamera
            layers: [objectLayer, sceneLayer, debugLayer]
            frustumCullingEnabled: true
            faceCullingEnabled: true
            depthTestEnabled: true
        }
    }

    InputSettings {
        id: inputSettings
    }

    components: [renderSettings, inputSettings]

    //OrbitCameraController {
    //    camera: mainCamera
    //}

    //FirstPersonCameraController {
    //    camera: mainCamera
    //}

    MyCameraController {
        camera: mainCamera
        viewport: root.viewport
    }

    ViewCenter {
        camera: mainCamera
        layer: layers.debugLayer
    }

    RootEntity {
        camera: mainCamera
        layers: layers
        Component.onCompleted: completed()
    }
}
