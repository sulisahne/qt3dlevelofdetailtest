#include "GroundEntity.h"
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QPlaneMesh>

namespace ibh {
GroundEntity::GroundEntity(Qt3DCore::QEntity* parent)
    : Qt3DCore::QEntity(parent)
    , m_transform(new Qt3DCore::QTransform())
{
    auto* mesh = new Qt3DExtras::QPlaneMesh();
    mesh->setWidth(5000);
    mesh->setHeight(5000);
    mesh->setMeshResolution({ 2, 2 });

    m_transform->setRotationX(90);

    auto* material = new Qt3DExtras::QPhongMaterial();
    material->setDiffuse("grey");
    material->setAmbient("white");

    addComponent(mesh);
    addComponent(m_transform);
    addComponent(material);
}

Qt3DRender::QCamera* GroundEntity::camera() const
{
    return m_camera;
}

Layers* GroundEntity::layers() const
{
    return m_layers;
}

void GroundEntity::setCamera(Qt3DRender::QCamera* camera)
{
    if (m_camera == camera)
        return;

    if (m_camera)
        disconnect(m_camPosConnection);

    m_camera = camera;
    emit cameraChanged(m_camera);

    m_camPosConnection = connect(m_camera, &Qt3DRender::QCamera::positionChanged, [this](const auto& position) {
        m_transform->setTranslation({ position.x(), position.y(), 0.0f });
    });
}

void GroundEntity::setLayers(Layers* layers)
{
    if (m_layers == layers)
        return;

    m_layers = layers;
    emit layersChanged(m_layers);

    addComponent(layers->sceneLayer());
}
}
